ffmpeg \
	-f alsa \
	-ar 32000 -ac 2 -i hw:2 \
	-f mpegts \
	-codec:a mp2 -b:a 64k \
	-muxdelay 0.001 \
	https://audioserver-54321.herokuapp.com/video
	#http://localhost:4000/video
