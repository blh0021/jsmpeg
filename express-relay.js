const express = require('express');
const server = express();
const fileUpload = require('express-fileupload');
server.use(fileUpload());
var expressWs = require('express-ws')(server);
server.use(express.static('./public', {
  setHeaders: function(res, path, stat) {
    res.set('Cache-Control', 'no-store')
    res.set('Cache-Control', 'no-cache')
    res.set('Pragma', 'no-cache')
  }
}));
const PORT = process.env.PORT || 4000;
var wss = expressWs.getWss()

server.use('/', function(req, res, next) {
  next();
})

server.ws('/vws', function(req, res) {
  // initialize outgoing websocket
});

server.post('/upload', function(req, res) {
  if (!req.files)
    return res.status(400).send('No files were uploaded.');

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  console.log(req.files);
  let sampleFile = req.files.image;

  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv('./public/filename.jpg', function(err) {
    if (err)
      return res.status(500).send(err);

    res.send('File uploaded!');
  });
});

server.use('/video', function(req, res) {
  var start = Date.now();
  req.on('data', function(data) {

    wss.clients.forEach(function each(client) {
      try {
        client.send(data);
      } catch (e) {
        //console.log(e);
        console.log(Date.now() + "Error Sending to WS");
      }
    });
  });
  req.on('end', function() {
    res.send();
    console.log('close');
    var end = Date.now();
    console.log(start)
    console.log(end)
  });
});
server.listen(PORT, () => console.log(`Listening on ${ PORT }`));
