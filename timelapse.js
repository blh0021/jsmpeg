const fs = require('fs');
const request = require('request');

const filename = './test.jpeg';
const url='https://audioserver-54321.herokuapp.com/upload';
//const url='http://localhost:4000/upload';
function upload() {
  var formData = {
    image: fs.createReadStream(filename)
  };
  request.post({url:url, formData: formData}, function optionalCallback(err, httpResponse, body) {
    if (err) {
      return console.error('upload failed:', err);
    }
    console.log(new Date());
    //console.log('Upload successful!  Server responded with:', body);
  });
}

fs.watchFile(filename,{interval: 1000}, function(curr, prev) {
  upload();
});
/*
fs.watch('./test.jpeg', {}, (eventType, filename) => {
  if (filename) {
    //console.log(filename);
    var formData = {
      image: fs.createReadStream(filename)
    };
    request.post({url:'http://localhost:4000/upload', formData: formData}, function optionalCallback(err, httpResponse, body) {
      if (err) {
        return console.error('upload failed:', err);
      }
      console.log(new Date());
      //console.log('Upload successful!  Server responded with:', body);
    });
  }
});
*/
